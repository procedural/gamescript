// NOTE(Constantine): https://github.com/google/filament/issues/2146

#include <math.h>

float __powf_finite(float base, float exponent) {
  return powf(base, exponent);
}

float __logf_finite(float arg) {
  return logf(arg);
}

float __acosf_finite(float arg) {
  return acosf(arg);
}

float __atan2f_finite(float y, float x) {
  return atan2f(y, x);
}

